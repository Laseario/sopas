#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/types.h>
 
#ifndef SEMAFOROS_H
#define SEMAFOROS_H
 
#define ERR -1
#define OK (!(ERR))
 
/***************************************************************
Nombre: Inicializar_Semaforo.
Descripcion:Inicializa los semaforos indicados.
Entrada:
 int semid: Identificador del semaforo.
 unsigned short *array: Valores iniciales.
Salida:
int: OK si todo fue correcto, ERROR en caso de error.
************************************************************/
int Inicializar_Semaforo(int semid, unsigned short *array){
     
    union semun {
        int val;
        struct semid_ds *semstat;
        unsigned short *array;
    } arg;
     
    if ( semid==-1 || array==NULL ) {
        errno = EINVAL;     
        return ERR; 
    }
    arg.array=array;
     
    return semctl(semid,ERR,SETALL,arg);
}
 
 
/***************************************************************
Nombre: Borrar_Semaforo.
Descripcion: Borra un semaforo.
Entrada:
 int semid: Identificador del semaforo.
Salida:
int: OK si todo fue correcto, ERROR en caso de error.
***************************************************************/
int Borrar_Semaforo(int semid){
    if ( semid==-1 ) {
        errno = EINVAL;     
        return ERR; 
    }
     
    /*Eliminamos todo el array*/
    return semctl (semid, 0, IPC_RMID); /*return semctl (semid, 1, IPC_RMID, 0);*/
}
 
 
 
/***************************************************************
Nombre: Crear_Semaforo.
Descripcion: Crea un semaforo con la clave y el tamaño 
especificado.Lo inicializa a 0.
Entrada:
 key_t key: Clave precompartida del semaforo.
 int size: Tamaño del semaforo.
 int *semid: Identificador del semaforo.
Salida:
 int: ERROR en caso de error, 0 si ha creado el semaforo,
 * 1 si ya estaba creado.
**************************************************************/
int Crear_Semaforo(key_t key, int size, int *semid){
    /*Controlamos error en los argumentos.*/
    if(size<1 || semid==NULL) {
        errno = EINVAL;     
        return ERR; 
    }
     
    /*Creacion del semaforo.*/
    *semid=semget(key, size, IPC_CREAT | IPC_EXCL | SHM_R | SHM_W );
    if(*semid==-1) {
        if(errno=EEXIST) {
            return 1;       
        }else{
            return ERR;     
            }
    }
 
    return 0;   
}
 
 
 
/**************************************************************
Nombre: Down_Semaforo
Descripcion:Baja el semaforo indicado
Entrada:
 int semid: Identificador del semaforo.
 int num_sem: Semaforo dentro del array.
 int undo: Flag de modo persistente pese a finalización abrupta.
Salida:
 int: OK si todo fue correcto, ERROR en caso de error.
***************************************************************/
int Down_Semaforo(int id, int num_sem, int undo){
    /*estructuras definidas en el archivo de cabecera <sys/sem.h>*/
    struct sembuf sem_oper;
     
    if(id==-1){
        errno = EINVAL;     
        return ERR;
    }
     
    sem_oper.sem_num=num_sem;
    /*operacion de down*/
    sem_oper.sem_op=-1;
     
    if(undo==1) {
        sem_oper.sem_flg=SEM_UNDO;  
    }
     
    return semop(id,&sem_oper,1);
     
     
}
/***************************************************************
Nombre: DownMultiple_Semaforo
Descripcion: Baja todos los semaforos del array indicado por active.
Entrada:
 int semid: Identificador del semaforo.
 int size: Numero de semaforos del array.
 int undo: Flag de modo persistente pese a finalización abrupta.
 int *active: Semaforos involucrados.
Salida:
int: OK si todo fue correcto, ERROR en caso de error.
***************************************************************/
int DownMultiple_Semaforo(int id,int size,int undo,int *active){
     
    if(id==-1 || size<0){
        errno = EINVAL;     
        return ERR;
    }
 
    int i=0;
    /*Bucle que itera para hacer down en cada semaforo.*/
    for(i=0;i<size;i++){
        if (Down_Semaforo(id,active[i],undo)==ERR) {
            return ERR;     
        }   
    }
    return OK;
 
    }
     
     
/**************************************************************
Nombre:Up_Semaforo
Descripcion: Sube el semaforo indicado
Entrada:
 int semid: Identificador del semaforo.
 int num_sem: Semaforo dentro del array.
 int undo: Flag de modo persistente pese a finalizacion abupta.
Salida:
 int: OK si todo fue correcto, ERROR en caso de error.
***************************************************************/
int Up_Semaforo(int id, int num_sem, int undo){
    struct sembuf sem_oper;
     
    if(id==-1){
        errno = EINVAL;     
        return ERR;
    }
    /*asignamos el numero del semaforo.*/
    sem_oper.sem_num=num_sem;
    /*operacion de Up.*/
    sem_oper.sem_op=1;
     
    if(undo==1){
        sem_oper.sem_flg=SEM_UNDO;  
    }
     
     
    return semop(id,&sem_oper,1);
}
 
 
/***************************************************************
Nombre: UpMultiple_Semaforo
Descripcion: Sube todos los semaforos del array indicado 
por active.
Entrada:
 int semid: Identificador del semaforo.
 int size: Numero de semaforos del array.
 int undo: Flag de modo persistente pese a finalización 
abrupta.
 int *active: Semaforos involucrados.
Salida:
int: OK si todo fue correcto, ERROR en caso de error.
***************************************************************/
int UpMultiple_Semaforo(int id,int size, int undo, int *active){
     
    if(id==-1 || size<0){
        errno = EINVAL;     
        return ERR;
    }
 
    int i=0;
    /*Bucle que itera para hacer up en cada semaforo.*/
    for(i=0;i<size;i++){
        if (Up_Semaforo(id,active[i],undo)==ERR) {
            return ERR;     
        }   
    }
    return OK;
}
 
 
#endif
