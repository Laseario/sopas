/**
 * @file cadena_montaje.c
 * @author Jorge Antolin Garcia Martinez <jorgeantolin.garcia@estudiante.uam.es>
 * @author Lucas Milla Lopez-Asiain <lucas.milla@estudiante.uam.es>
 * @date 24/04/2015
 * @brief Ejercicio 1 de la practica 4 de SOPER.
 * 
 * Este programa implementa una cadena de montaje usando colas de mensajes de UNIX.
 * Esta cadena de montaje está compuesta por tres procesos, cada uno realiza una funcion
 * conectada.
 * 
 */

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/msg.h> 


#define ERR -1
#define OK 0

#define N 33
#define buffer_len 4096

typedef enum { false, true } bool;

/**Funcion auxiliar que convierte un String a mayusculas.*/
char *convertirMayus(char *str){
    char *newstr, *p;
    p = newstr = strdup(str);
    while(*p++=toupper(*p));

    return newstr;
}


typedef struct _Mensaje{
	/**Campo obligatorio a long que identifica el tipo de mensaje*/
        long id;
 
	/**Informacion a transmitir en el mensaje*/ 
        
        /**end indica si es el ultimo trozo de mensaje a leer.*/
	bool end;
	char buffer_string[buffer_len];

	}mensaje;


int main(int argc, char** argv){
    char* mayus;
    key_t clave; 
    int i, estado, contador=0, msqid, lectura=0;
    pid_t aux;
    mensaje msg;

    if(argc!=3){
	printf("Error en los parametros de entrada.");
	return ERR;
    }


    clave = ftok ("/bin/cat", N); 
    if (clave == (key_t) -1) 
    { 
    perror("Error al obtener clave para cola mensajes\n"); 
    exit(EXIT_FAILURE); 
    } 

    /* 
    * Se crea la cola de mensajes y se obtiene un identificador para ella. 
    * El IPC_CREAT indica que cree la cola de mensajes si no lo está. 
    * 0600 son permisos de lectura y escritura para el usuario que lance 
    * los procesos. Es importante el 0 delante para que se interprete en octal. 
    */ 
    
    msqid = msgget (clave, 0600 | IPC_CREAT); 
    if (msqid == -1) 
    { 
    perror("Error al obtener identificador para cola mensajes"); 
    return(0); 
    }

    /**Creamos los tres procesos de la cadena de montaje, cada uno especializado en una función.*/
    for(i=0; i< 3; i++){
        aux = fork();
        if(aux<0){
            fprintf(stdout, "Error en la llamada la funcion fork\n");
            return -1;
        }       
        if(aux==0){
            break;
        }   
 
    }
	if(aux>0){   
        for(i=0; i<3; i++){
            wait(&estado);
        }

	/* 
	* Se borra y cierra la cola de mensajes. 
	* IPC_RMID indica que se quiere borrar. El puntero del final son datos 
	* que se quieran pasar para otros comandos. IPC_RMID no necesita datos, 
	* así que se pasa un puntero a NULL. 
	*/
	msgctl (msqid, IPC_RMID, (struct msqid_ds *)NULL); 
 	exit(1);
    }
    
    /**Implementamos el primer proceso: lee de un fichero f1 y escribe en la primera cola de mensajes 
      *trozos del fichero de longitud máxima 4KB (4096 * sizeof(char)).*/
    if(i==0){

	    FILE *fo = fopen(argv[1], "r");
		msg.end=false;
	    if(fo==NULL){
		fprintf(stdout, "Error al abrir el fichero f1.");
		return 1;
	    }

	    while(!feof(fo)){

		    lectura=fread(msg.buffer_string,4096,1,fo);
		    if(lectura==-1){
			perror("fread");
			fprintf(stdout, "Error en la lectura del fichero 1.");
		    }
		    
		    /**Procedemos a escribir el mensaje en la cola.*/
		    msg.id=2;
		if (feof(fo))
			msg.end=true;

		    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){
			perror("msgsnd");
			fprintf(stdout, "Error al enviar el mensaje.");
			exit(-1);
		    }
	    }
		printf("terminado 0,%d\n",msg.end);
	    fclose(fo);	
	    exit(0);
    }


    /**El segundo proceso lee de la cola de mensajes cada trozo del fichero y reemplaza las letras minúsculas 
    por letras mayúsculas. Una vez realizada esta transformación, escribe el contenido en la cola de mensajes.*/
    if(i==1){
    		msg.end=false;

	    /**Bucle que va leyendo de la cola de mensajes cada trozo del fichero.*/
	    while(!msg.end) {
		    /**En este caso se quiere que el programa quede bloqueado hasta que llegue un mensaje de tipo 2.*/
		    if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 2, 0)==-1){
			    fprintf(stdout, "Error al leer de la cola de mensajes.");
			    exit(-1);
		    }

		    msg.id=3;
		    mayus=convertirMayus(msg.buffer_string);
			strcpy(msg.buffer_string,mayus);

		    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) ,0)==-1){
			perror("msgsnd");    
			fprintf(stdout, "Error al enviar el mensaje en mayusculas.");
			exit(-1);
		    }
	    }
		printf("terminado 1\n");
	    exit(1);
    }

    /**El último proceso lee de la cola el trozo de memoria y lo vuelca al fichero f2.*/
    if(i==2){

	    FILE *fd = fopen(argv[2], "w");

	    if(fd==NULL){
		fprintf(stdout, "Error al abrir el fichero f2.");
		return 1;
	    }
		msg.end=false;
	    while(!msg.end){
		    if(msgrcv(msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 3, 0)==-1){
			    fprintf(stdout, "Error al leer de la cola de mensajes.");
			    exit(-1);
		    }
			if(fputs(msg.buffer_string,fd)==-1){
			    fprintf(stdout, "Error al escribir en el fichero 2.");
			    exit(-1);
		    }
	    }
		printf("terminado 2\n");
	    fclose(fd);

	    exit(0);
    }

}

