/**
 * @file Ejercicio2.c
 *
 * @author Jorge Garcia Martinex
 * @author Lucs Milla Lopez-Asiain
 *
 * @date 24/04/2015
 *
 * @brief Ejercicio 2 de la Practica 4 de SOPER
 *
 * Este programa implementa la simulacion de un exmaen con 2 clases, 2 profesores que acomodan
 * a los alumnos por clase; y un profesor_examen por clase encargado de recoger y despachar
 * alumnos. Todos estos seran procesos que  se comunicarán con el gestor, el que realiza todas las
 * acciones durante el examen. El proceso padre.
 *
 **/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/msg.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h> 
#include <sys/sem.h> 
#include <errno.h> 
#include <sys/shm.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include "Semaforos.h"
#include <pthread.h>
#include <unistd.h>
 
 
#define FILEKEY "/bin/ls" 
#define KEY 1300 
#define SEMKEY 75798

#define N 33
 
#define ERR -1
#define OK (!(ERR))
 
typedef enum {
    false, true
} bool;
 /* Estructura asiento con el pid_t del alumno y una variable de tipo boolean para saber si está ocupado*/
typedef struct _Asiento {
    pid_t alumno;
    bool ocupado;
} Asiento;
 /* Estrucutra aula, con el numero de asientos, un array de asientos, los asientos libres, los dos profesores y el profesor_examen*/
typedef struct _Aula {
    int numAsientos;
    Asiento *asientos;
    double asientos_libres;
    pid_t *profesor;
    pid_t profesor_examen;
} Aula;

typedef struct _Mensaje{
    /*Campo obligatorio a long que identifica el tipo de mensaje*/
        long id;
    /*Informacion a transmitir en el mensaje*/
        
        pid_t valor;
        bool accion;
        int clase;
 
    }mensaje;
 
 /* Variables globales*/
Aula * buffer; /* shared buffer */
int id_gestor;
mensaje msg;
int msqid;
int sem;


void  funcion_alumno(int clase);

void manejador_SIGUSR1_gestor (int sig);
void manejador_SIGUSR1_alumnos(int sig);
void manejador_SIGUSR1_profesores_0 (int sig);
void manejador_SIGUSR1_profesores_1 (int sig);
void manejador_SIGUSR1_profesor_examen_0 (int sig);
void manejador_SIGUSR1_profesor_examen_1 (int sig);
void manejador_SIGUSR2_gestor (int sig); 
void manejador_SIGUSR2_alumnos(int sig);

 

int main(int argc, char** argv) {
    int numAlumnos;
    int key, id_zone1, id_zone2, id_zone3, id_zone4, id_zone5;
    int i, estado;
    pid_t aux;
    unsigned short buf[6] = {1, 1, 1, 1, 1, 1};/* Array de inicialización del semáforo*/
    int clase;
    key_t clave;

    id_gestor = getpid();
    
    /* Memoria compartida para la estructura general buffer*/
    /*******************************************************/
    /* Key to shared memory */
    key = ftok(FILEKEY, KEY);
    if (key == -1) {
        fprintf(stderr, "Error with key \n");
        return -1;
    }
 
    /* We create the shared memory */
    id_zone1 = shmget(key, sizeof(Aula), IPC_CREAT | 0666);
    if (id_zone1 == -1) {
        fprintf(stderr, "Error with id_zone \n");
        exit(-1);
    }
    printf("ID zone shared memory: %i\n", id_zone1);
 
 
    /* we declared to zone to share */
    buffer = (Aula*)shmat(id_zone1, NULL, 0666);
    if (buffer == NULL) {
        fprintf(stderr, "Error reserve shared memory \n");
        exit(EXIT_FAILURE);
    }
 
    /* Creacion de los 6 semáforos necesarios*/
    if (Crear_Semaforo(SEMKEY, 6, &sem) == ERR) {
        perror("Error al crear semaforo");
    }
 
    /* Inicializacion de todos los semaforos a 1*/
    if (Inicializar_Semaforo(sem, buf) == ERR) {
        perror("Error inicializar sem");
        Borrar_Semaforo(sem);
        exit(EXIT_FAILURE);
    }	
	
	/* Creación de la cola de mensajes*/
    clave = ftok ("/bin/cat", N); 
    if (clave == (key_t) -1) 
    { 
    perror("Error al obtener clave para cola mensajes\n"); 
    exit(EXIT_FAILURE); 
    } 
 
    /* 
    * Se crea la cola de mensajes y se obtiene un identificador para ella. 
    * El IPC_CREAT indica que cree la cola de mensajes si no lo está. 
    * 0600 son permisos de lectura y escritura para el usuario que lance 
    * los procesos. Es importante el 0 delante para que se interprete en octal. 
    */
     
    msqid = msgget (clave, 0600 | IPC_CREAT); 
    if (msqid == -1) 
    { 
    perror("Error al obtener identificador para cola mensajes"); 
    return(0); 
    }
 
    fprintf(stdout, "Introduce el numero de asientos del aula 1:\n");
    fscanf(stdin, "%d", &buffer[0].numAsientos);
    fprintf(stdout, "Introduce el numero de asientos del aula 2:\n");
    fscanf(stdin, "%d", &buffer[1].numAsientos);
    fprintf(stdout, "Introduce el numero de alumnos:\n");
    fscanf(stdin, "%d", &numAlumnos);
    
    buffer[0].numAsientos= ceil(buffer[0].numAsientos/2);
    buffer[1].numAsientos= ceil(buffer[1].numAsientos/2);
    
    buffer[0].asientos_libres = buffer[0].numAsientos;
    buffer[1].asientos_libres = buffer[1].numAsientos;
 

    /* Memoria compartida para buffer[0].asientos*/
    /*******************************************************/
     

    /* Key to shared memory */
    key = ftok(FILEKEY, 1301);
    if (key == -1) {
        fprintf(stderr, "Error with key \n");
        return -1;
    }
 
    /* We create the shared memory */
    id_zone2 = shmget(key, sizeof(Asiento)*buffer[0].numAsientos, IPC_CREAT | 0666);
    if (id_zone2 == -1) {
        fprintf(stderr, "Error with id_zone \n");
        exit(-1);
    }
    printf("ID zone shared memory: %i\n", id_zone2);
 
 
    /* we declared to zone to share */
    buffer[0].asientos = (Asiento*)shmat(id_zone2, NULL, 0666);
    if (buffer[0].asientos == NULL) {
        fprintf(stderr, "Error reserve shared memory \n");
        exit(EXIT_FAILURE);
    }



    /* Memoria compartida para buffer[1].asientos*/
    /*******************************************************/



    /* Key to shared memory */
    key = ftok(FILEKEY, 1302);
    if (key == -1) {
        fprintf(stderr, "Error with key \n");
        return -1;
    }
 
    /* We create the shared memory */
    id_zone3 = shmget(key, sizeof(Asiento)*buffer[1].numAsientos, IPC_CREAT | 0666);
    if (id_zone3 == -1) {
        fprintf(stderr, "Error with id_zone \n");
        exit(-1);
    }
    printf("ID zone shared memory: %i\n", id_zone3);
 
 
    /* we declared to zone to share */
    buffer[1].asientos = (Asiento*)shmat(id_zone3, NULL, 0666);
    if (buffer[1].asientos == NULL) {
        fprintf(stderr, "Error reserve shared memory \n");
        exit(EXIT_FAILURE);
    }
    

      /* Memoria compartida para buffer[0].profesor*/
    /*******************************************************/
     /* Key to shared memory */
    key = ftok(FILEKEY, 1303);
    if (key == -1) {
        fprintf(stderr, "Error with key \n");
        return -1;
    }
 
    /* We create the shared memory */
    id_zone4 = shmget(key, sizeof(pid_t)*2, IPC_CREAT | 0666);
    if (id_zone4 == -1) {
        fprintf(stderr, "Error with id_zone \n");
        exit(-1);
    }
    printf("ID zone shared memory: %i\n", id_zone4);
 
 
    /* we declared to zone to share */
    buffer[0].profesor = (pid_t*)shmat(id_zone4, NULL, 0666);
    if (buffer[0].profesor == NULL) {
        fprintf(stderr, "Error reserve shared memory \n");
        exit(EXIT_FAILURE);
    }
    
  /* Memoria compartida para buffer[1].profesor*/
    /*******************************************************/
     /* Key to shared memory */
    key = ftok(FILEKEY, 1304);
    if (key == -1) {
        fprintf(stderr, "Error with key \n");
        return -1;
    }
 
    /* We create the shared memory */
    id_zone5 = shmget(key, sizeof(pid_t)*2, IPC_CREAT | 0666);
    if (id_zone5 == -1) {
        fprintf(stderr, "Error with id_zone \n");
        exit(-1);
    }
    printf("ID zone shared memory: %i\n", id_zone5);
 
 
    /* we declared to zone to share */
    buffer[1].profesor = (pid_t*)shmat(id_zone5, NULL, 0666);
    if (buffer[1].profesor == NULL) {
        fprintf(stderr, "Error reserve shared memory \n");
        exit(EXIT_FAILURE);
    }
    
    
 
    /* Mapeo de los manejadores de señales del gestor. SIGUSR1 y SIGUSR2*/
    if(signal (SIGUSR1, manejador_SIGUSR1_gestor)==SIG_ERR){ 
                perror("signal SIGUSR1"); 
                exit(EXIT_FAILURE); 
            }
    if(signal (SIGUSR2, manejador_SIGUSR2_gestor)==SIG_ERR){ 
                perror("signal SIGUSR2"); 
                exit(EXIT_FAILURE); 
            }

	/* Creación de los dos profesores_examen y mapeo de sus correspondientes manejadores de SIGUSR1*/
    for (i = 0; i < 2; i++) {
        aux = fork();
        if (aux < 0) {
            fprintf(stdout, "Error en la llamada la funcion fork\n");
            exit(-1);
        }
        if (aux == 0) {
	    if(i==0){
	        buffer[i].profesor_examen = getpid();/* Asignamos el id del proceso al profesor_examen*/
		if(signal (SIGUSR1, manejador_SIGUSR1_profesor_examen_0)==SIG_ERR){ 
					perror("signal SIGUSR1"); 
					exit(EXIT_FAILURE); 
	        }
	    }else{
                buffer[i].profesor_examen = getpid();/* Asignamos el id del proceso al profesor_examen*/
		if(signal (SIGUSR1, manejador_SIGUSR1_profesor_examen_1)==SIG_ERR){ 
					perror("signal SIGUSR1"); 
					exit(EXIT_FAILURE); 
	        }
	    }
	    
 
            while (1) {/* Están perpetuamente en espera de recibir señales hasta que reciba SIGTERM*/
                pause();
            }
        }
 
    }

	/* Creación de los dos profesores de cada clase y  mapeo de sus correspondientes manejadores de SIGUSR1*/
    for (i = 0; i < 4; i++) {
        aux = fork();
        if (aux < 0) {
            fprintf(stdout, "Error en la llamada la funcion fork\n");
            exit(-1);
        }
        if (aux == 0) {


            if (i < 2) {
		
	     if(signal (SIGUSR1, manejador_SIGUSR1_profesores_0)==SIG_ERR){ 
		        perror("signal SIGUSR1"); 
		        exit(EXIT_FAILURE); 
		    }
                buffer[0].profesor[i] = getpid();/* Asignamos el id del proceso al profesor*/
            } else {

	     if(signal (SIGUSR1, manejador_SIGUSR1_profesores_1)==SIG_ERR){ 
		        perror("signal SIGUSR1"); 
		        exit(EXIT_FAILURE); 
		    }
                buffer[1].profesor[i % 2] = getpid();/* Asignamos el id del proceso al profesor*/
            }
 
            while (1) {/* Están perpetuamente en espera de recibir señales hasta que reciba SIGTERM*/
                pause(); 
            }
 
            exit(0);
        }
 
    }
 
    for (i = 0; i < numAlumnos; i++) {/* Creación de todos los alumnos a partir del gestor*/

	    aux = fork();
        if (aux < 0) {
            fprintf(stdout, "Error en la llamada la funcion fork\n");
            exit(-1);
        }
        if (aux == 0) {/* Mapeo de las señales SIGUSR1 y SIGUSR2*/
             if(signal (SIGUSR1, manejador_SIGUSR1_alumnos)==SIG_ERR){ 
		        perror("signal SIGUSR1"); 
		        exit(EXIT_FAILURE); 
		    }
             if(signal (SIGUSR2, manejador_SIGUSR2_alumnos)==SIG_ERR){ 
		        perror("signal SIGUSR2"); 
		        exit(EXIT_FAILURE); 
		    }
	    srand(i);/* cambiamos la semilla del rand*/
	    funcion_alumno(rand()%2);/* Rutina de los alumnos*/

	    
        }
   }

   for(i = 0; i < numAlumnos*3; i++){/* El proceso gestor recibe numAlumnos*3 señales en total si todo ha ido correctamente*/
	pause();
    }
	
	/* Terminación de los procesos hijos*/
	for(i=0;i<2;i++){
		if(kill(buffer[i].profesor_examen, SIGTERM)==-1){
		perror("Error kill signal SIGUSR1 entre alumno y profesor_examen"); 
			exit(EXIT_FAILURE);     
		}
		if(kill(buffer[i].profesor[0], SIGTERM)==-1){
		perror("Error kill signal SIGUSR1 entre alumno y profesor_examen"); 
			exit(EXIT_FAILURE);     
		}
		if(kill(buffer[i].profesor[1], SIGTERM)==-1){
		perror("Error kill signal SIGUSR1 entre alumno y profesor_examen"); 
			exit(EXIT_FAILURE);     
		}
	}
	for(i = 0; i < numAlumnos; i++){/* El proceso gestor espera por todos sus alumnos*/
	wait(&estado);
    }
	/* Liberación de memoria compartida de todos los espacios reservados*/
    shmdt(buffer[0].asientos);
    shmdt(buffer[1].asientos);
    shmdt(buffer[0].profesor);
    shmdt(buffer[1].profesor);
    shmdt(buffer);
    shmctl(id_zone1, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(id_zone2, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(id_zone3, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(id_zone4, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(id_zone5, IPC_RMID, (struct shmid_ds *)NULL);
	
    Borrar_Semaforo(sem); /* Se borra el semaforo creado*/
	/* Se libera la cola de mensajes*/
    msgctl (msqid, IPC_RMID, (struct msqid_ds *)NULL); 
    exit(1);
 
}
 /***************************************************************************************************/
void  funcion_alumno(int clase){

if(buffer[clase].asientos_libres/buffer[clase].numAsientos<0.15){/* Control del 15% de sitios libres en una clase*/
	if(clase==0){
		clase=1;
	}
	else{
		clase=0;
	}
}

srand(getpid());/* Cambiamos la semilla del rand*/

int i;
msg.id=1;/* Id = 1 , para mensajes desde los alumnos al gestor*/
msg.valor=getpid();/* Envia su pid y su clase*/
msg.clase=clase;
if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){/* Se envia el mensaje a la cola*/
            perror("msgsnd");
            fprintf(stdout, "Error al enviar el mensaje.");
            exit(-1);
            }
	 
if(kill(id_gestor, SIGUSR1)==-1){/* Se envia señal al gestor para que lea mensaje*/
	        perror("Error kill signal SIGUSR1 entre alumno y gestor"); 
	        exit(EXIT_FAILURE);     
	}

 Down_Semaforo(sem, clase, 0);/* Se baja el semáforo de la clase para que nadie más pueda entrar al mismo tiempo*/
 pause();/* Se espera a recibir la señal de poder entrar*/
 sleep(rand()%5);
 
 msg.id=4;
 msg.valor=getpid();
 printf("Alumno %d haciendo el examen en clase %d \n", getpid(),clase);
 sleep(10);
 Down_Semaforo(sem, clase+4, 0);/* Se baja el semáforo para salir de la clase y que nadie más llame al mismo tiempo al profesor_examen*/
if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){
	    perror("msgsnd");
	    fprintf(stdout, "Error al enviar el mensaje.");
	    exit(-1);
	    }

 if(kill(buffer[msg.clase].profesor_examen, SIGUSR1)==-1){/* Envio de señal al profesor_examen adecuado*/
		perror("Error kill signal SIGUSR1 entre alumno y profesor_examen"); 
			exit(EXIT_FAILURE);     
		}
 pause();/* Espera a la señal de poder salir de la clase*/
 
exit(0);   /* Terminación del proceso alumno*/
    
}
void manejador_SIGUSR1_gestor (int sig){

    if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 1, 0)==-1){/* Se recibe el mensage con el id y la clase del alumno*/
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
            }  
    printf("Gestor recibe peticion de entrada de  %d en clase %d \n", msg.valor,msg.clase);
    srand(msg.valor);/* Se cambia la semilla para decidir aleatoriamente a que profesor de los dos que hay despertar*/
    msg.id=2;/* Id = 2, mensages entre el gestor y los profesores*/

    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){/* Envio del mismo mensaje a la cola*/
            perror("msgsnd");
            fprintf(stdout, "Error al enviar el mensaje.");
            exit(-1);
            }
    if(kill(buffer[msg.clase].profesor[rand()%2], SIGUSR1)==-1){/* Envío de la señal al profesor para que lea el mensage*/
                perror("Error kill signal SIGUSR1"); 
                exit(EXIT_FAILURE);     
        }
} 

void manejador_SIGUSR2_gestor (int sig){

    if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 3, 0)==-1){/* Se recibe el mensaje con el sitio que hay que ocuparç/desocupar y la clase*/
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
       }


    buffer[msg.clase].asientos[msg.valor].ocupado=msg.accion; /* Se cambia el estado del asiento*/
    if(msg.accion==true){/* Se ocupa un asiento*/
	      printf("Gestor reorganiza clase %d añadiendo \n", msg.clase);
	    buffer[msg.clase].asientos_libres--;/* Se reducen los asientos libres*/
	    Up_Semaforo(sem, msg.clase, 0);/* Se sube el semáforo para que pueda entrar gente a la clase*/
            Up_Semaforo(sem, 2, 0);/* Se sube el semáforo de conversaciones profesor-gestor*/
        
    }
    else{/* Se desocupa un asiento*/
        printf("Gestor reorganiza clase %d sacando \n", msg.clase);
	buffer[msg.clase].asientos_libres++;/* Se incrementan los asientos libres*/
	Up_Semaforo(sem, msg.clase+4, 0);/* Se sube el semáforo para que pueda seguir saliendo gente de la clase*/
	Up_Semaforo(sem, 3, 0);/* Se sube el semaforo de conversaciones profesor_examen-gestor*/
    }  
} 

void manejador_SIGUSR1_profesor_examen_0 (int sig){

if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 4, 0)==-1){/* Se recibe el id del alumno al que recoger el examen*/
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
            }
  int i;
  int asiento;
  int id_alumno = msg.valor;
  Down_Semaforo(sem, 3, 0); /* Se baja el semaforo de conversaciones gestor-profesor_examen*/

	for(i=0; i<buffer[0].numAsientos; i++){/* Busqueda del asiento en el que se encuentra el alumno*/
		if(buffer[0].asientos[i].alumno==msg.valor){
			asiento=i;
			break;
        	}
        }
	if(asiento!=i){
		perror("Error recogiendo examen\n");
	}
printf("Profesor %d examen aula 0 recoge examen a %d en asiento %d\n", getpid(),id_alumno,i);
buffer[0].asientos[asiento].alumno=0;/* Borrado del id del alumno en dicho asiento*/


  

   msg.id=3;/* Id = 3, para mensajes entre el profesores y el gestor*/
   msg.valor=asiento;
   msg.accion=false;
    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){/* Envío del asiento que debe cambiar el estado el gestor*/
                        perror("msgsnd");
                        fprintf(stdout, "Error al enviar el mensaje.");
                        exit(-1);
                }

   if(kill(id_gestor, SIGUSR2)==-1){/* Envio de la señal al gestor de que lea el mensaje*/
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}
 if(kill(id_alumno, SIGUSR2)==-1){
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); /* Envío de la señal al alumno de que puede salir*/
		        exit(EXIT_FAILURE);     
		}
  
   
} 

/* Lo mismo pero para profesores del aula 1*/
void manejador_SIGUSR1_profesor_examen_1 (int sig){

if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 4, 0)==-1){
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
            }
  int i;
  int asiento;
  int id_alumno = msg.valor;
  Down_Semaforo(sem, 3, 0); 

	for(i=0; i<buffer[1].numAsientos; i++){
		if(buffer[1].asientos[i].alumno==msg.valor){
			asiento=i;
			break;
        	}
        }
	if(asiento!=i){
		perror("Error recogiendo examen\n");
	}
printf("Profesor %d examen aula 1 recoge examen a %d en asiento %d\n", getpid(),id_alumno,i);
buffer[1].asientos[asiento].alumno=0;


  

   msg.id=3;
   msg.valor=asiento;
   msg.accion=false;
    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){
                        perror("msgsnd");
                        fprintf(stdout, "Error al enviar el mensaje.");
                        exit(-1);
                }

   if(kill(id_gestor, SIGUSR2)==-1){
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}
 if(kill(id_alumno, SIGUSR2)==-1){
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}
  
   
} 
    
void manejador_SIGUSR1_profesores_0 (int sig){
 
   if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 2, 0)==-1){/* Recibe el id del alumno que quiere entrar a clase*/
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
            }
    Down_Semaforo(sem, 2, 0);/* Baja el semafor de conversaciones profesor-gestor*/
    int i;
    int asiento;
    int id_alumno=msg.valor;
    for(i=0; i<buffer[0].numAsientos; i++){/* Búsqueda de un primer asinto libre*/
	if(buffer[0].asientos[i].ocupado==false){
		asiento = i;
		break;
	}
    }
 
   printf("Profesor %d  aula 0 sienta a %d en asiento %d\n", getpid(),id_alumno,asiento);
   buffer[0].asientos[asiento].alumno=id_alumno;/* Asignación del id del alumno al asiento libre*/
   
  
   msg.id=3;/* Id = 3, para mensajes entre el profesores y el gestor*/
   msg.valor=asiento;
   msg.accion=true;
   
    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){/* Envio del mensaje al gestor de ocupar un asiento*/
                        perror("msgsnd");
                        fprintf(stdout, "Error al enviar el mensaje.");
                        exit(-1);
                }

   if(kill(id_gestor, SIGUSR2)==-1){/* Envio de la señal para que el gestor lea el mensaje*/
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}
   if(kill(id_alumno, SIGUSR1)==-1){/* Envio de la señal para que el alumno entre al aula*/
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}

}
/* Lo miso que el anterior pero para el aula 1*/
void manejador_SIGUSR1_profesores_1 (int sig){
 
   if(msgrcv (msqid, (struct msgbuf *)&msg, sizeof(mensaje) - sizeof(long), 2, 0)==-1){
                fprintf(stdout, "Error al leer de la cola de mensajes.");
                exit(-1);
            }
    Down_Semaforo(sem, 2, 0);
    int i;
    int asiento;
    int id_alumno=msg.valor;
    for(i=0; i<buffer[1].numAsientos; i++){
	if(buffer[1].asientos[i].ocupado==false){
		asiento = i;
		break;
	}
    }
 
   printf("Profesor %d  aula 1 sienta a %d en asiento %d\n", getpid(),id_alumno,asiento);
   buffer[1].asientos[asiento].alumno=id_alumno;
   
  
   msg.id=3;
   msg.valor=asiento;
   msg.accion=true;
   
    if(msgsnd (msqid, (struct msgbuf *) & msg, sizeof(mensaje) - sizeof(long) , 0)==-1){
                        perror("msgsnd");
                        fprintf(stdout, "Error al enviar el mensaje.");
                        exit(-1);
                }

   if(kill(id_gestor, SIGUSR2)==-1){
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}
   if(kill(id_alumno, SIGUSR1)==-1){
		        perror("Error kill signal SIGUSR2 entre alumno y gestor"); 
		        exit(EXIT_FAILURE);     
		}

}

/* El alumno pone por pantalla su id y lo que hace*/

void manejador_SIGUSR1_alumnos (int sig){
 printf("Alumno %d entrando en clase\n", getpid());
}

void manejador_SIGUSR2_alumnos (int sig){
 printf("Alumno %d saliendo de clase\n", getpid());
}



