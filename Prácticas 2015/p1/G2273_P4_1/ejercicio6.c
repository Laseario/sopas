/** * @brief Solcion al ejercicio 6 de la Practica 1 de SOPER
    * 
    * Este programa sirve para comprobar si la memoria reservada
    * por un padre se reserva tambien en el hijo, y si
    * el hijo escribe en ella, comprobar si el padre puede acceder
    * al valor introducido por el hijo.
    * @file ejercicio6.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 


#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

#define NUMCARACTERES 20

int main(){

	int i, estado;
	pid_t aux;
	char *cadena= (char*) malloc(sizeof(char)*NUMCARACTERES);
	strcpy(cadena, "Padre");
	aux = fork();
	
	if(aux<0){
		fprintf(stdout,"Error en la llamada al proceso fork\n");
		return -1;
	}
	else if(aux==0){
		fprintf(stdout,"Introduce una palabra para el hijo\n");
		fscanf(stdin,"%s", cadena);
		fprintf(stdout,"El valor introducido al hijo es:%s\n", cadena);
	}
	else{
		wait(&estado);
		fprintf(stdout,"El valor del padre es:%s\n", cadena);
	}
	free(cadena);
	return 0;
}
	
