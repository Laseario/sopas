/** * @brief Solcion al ejercicio 9 de la Practica 1 de SOPER
    * 
    * Este programa genera 10 procesos hijos a partir de un padre
    * y se comprueba la comunicacion entre el padre y cada hijo 
    * por medio de tuberias.
    * @file ejercicio9.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define NUMHIJOS 10

int main(){

	int i, estado, **fd, pipe_status, nbytes;
	pid_t childpid;
	char string1[100] = "Datos enviados a traves de la tuberia";
	char string2[100];
	char buf1[100];
	char buf2[100];

	fd = (int**) malloc(sizeof(int*)*20);
	if(fd==NULL){
		fprintf(stdout,"Error reservando memoria\n");
		return -1;
	}
	
	for(i=0; i<20; i++){
		fd[i] = (int*) malloc(sizeof(int)*2);
	}
		
	for(i=0; i< NUMHIJOS; i++){
		pipe_status= pipe(fd[2*i]);
		if(pipe_status==-1){
			fprintf(stdout,"Error creando la tuberia %d \n", i+1);
			return -1;
		}
		pipe_status= pipe(fd[(2*i)+1]);	
			if(pipe_status==-1){
				fprintf(stdout,"Error creando la tuberia %d \n", i+1);
				return -1;
			}
		childpid= fork();
		if(childpid>0){
			close(fd[2*i][0]);
			nbytes=write(fd[2*i][1],string1,strlen(string1));
			if(nbytes==-1){
				fprintf(stdout, "Error en la escritura de la tuberia1\n");
				return -1;
			}
		}		
		if(childpid==0){
			close(fd[2*i][1]);
			nbytes = read(fd[2*i][0],buf1,sizeof(buf1));	
			if(nbytes==-1){
				fprintf(stdout, "Error en la lectura de la tuberia1\n");
				return -1;
			}
			fprintf(stdout,"%s\n", buf1);		
			close(fd[(2*i)+1][0]);	
			sprintf(string2,"Datos devueltos a traves de la tuberia por el proceso PID=%d", getpid());
			nbytes=write(fd[(2*i)+1][1],string2,strlen(string2));
			if(nbytes==-1){
				fprintf(stdout, "Error en la escritura de la tuberia2\n");
				return -1;
			}
			exit(0);
			break;
		}
		else if(childpid>0){
			close(fd[(2*i)+1][1]);
			wait(&estado);
			nbytes= read(fd[(2*i)+1][0],buf2,sizeof(buf2));
			if(nbytes==-1){
				fprintf(stdout, "Error en la lectura de la tuberia2\n");
				return -1;
			}
			buf2[sizeof(buf2)]='\0';
			fprintf(stdout,"%s\n", buf2);
		}		

	}
	
	return 0;
}
	
