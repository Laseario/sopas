/** * @brief Solcion al ejercicio 5 de la Practica 1 de SOPER
    * 
    * Este programa genera 4 procesos hijos, el primero del
    * padre principal, el segundo del primer hijo, 
    * y así sucesivamente.
    * Después cada hijo muestra por pantalla su PID
    * y el de su padre.
    * @file ejercicio5.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 


#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#define NUMHIJOS 4

int main(){

	int i, estado;
	pid_t aux;

	for(i=0; i< NUMHIJOS; i++){
		aux = fork();
		if(aux<0){
			fprintf(stdout, "Error en la llamada la funcion fork\n");
			return -1;
		}		
		if(aux==0){
			fprintf(stdout,"%d %d\n", getpid(), getppid());
			
		}
		else{	
			wait(&estado);
			break;
		}	

	}

return 0;
}
	
