/** * @brief Solcion al ejercicio 8 de la Practica 1 de SOPER
    * 
    * Este programa genera 2 procesos hijos a partir de un padre.
    * El primer hijo ha de mostrar los procesos activos por pantalla
    * y el segundo debe ejecutar el comando pasado por argumento.
    * @file ejercicio8.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define NUMHIJOS 2

int main(int argc, char **argv){

	pid_t aux;
	int i, status;
	
	if(argc!=2){
		fprintf(stdout,"Introduce un programa ejecutable como argumento de entrada\n");
		return -1;	
	}
	for(i=0; i<NUMHIJOS;i++){
		aux = fork();

		if(aux<0){
			fprintf(stdout,"Error en la llamada al proceso fork\n");
			return -1;
		}

		else if(aux==0&&i==0){
			execlp("ps","ps", "-fu", getenv("USER"),NULL);	
		}
		else if(aux==0&&i==1){
			execlp(argv[1], argv[1], NULL);
		}
		wait(&status);
	}

	if(aux>0){
		for(i=0; i<NUMHIJOS;i++){
			wait(&status);
		}		
	}
	return 0;
}
