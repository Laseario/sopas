#include <stdio.h> 
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h> 
#include <sys/sem.h> 
#include <errno.h> 
#include <sys/shm.h>
#include "semaforos.h"

#define FILEKEY "/bin/cat" 
#define KEY 1300 
#define SEMKEY 75798
/*Funcion por la que entra un coche en el puente y lo bloquea para el resto de coches del sentido contrario*/
void LightSwitchOn(int bab,int sentido,int rec,int **contador);
/*Funcion que implementa la salida del ultimo coche en un sentido, para abrir el puente en el sentido contrario*/
void LightSwitchOff(int bab,int sentido,int rec,int **contador);

int main(){

    int i, pid;
    int sentido;
    int sem;
    int status;
    int *contador =NULL;
    unsigned short buf2[3]= {1,1,1};
    int id_zona;
    int key;

/* Key to shared memory */
     key = ftok(FILEKEY, KEY); 
     if (key == -1) { 
     fprintf (stderr, "Error with key \n"); 
     return -1; 
     } 
 /* We create the shared memory */
     id_zona= shmget (key, sizeof(int)*2, IPC_CREAT | IPC_EXCL |SHM_R | SHM_W); 
     if (id_zona== -1) { 
     fprintf (stderr, "Error with id_zone_padre \n"); 
     exit(-1);
    }
    
     contador = shmat (id_zona,(int *)0, 0); 
      if (contador == NULL) { 
      	fprintf (stderr, "Error reserve shared memory \n"); 
 	return -1;
      }

 /* Creacion de los 3 semáforos necesarios*/
    if(Crear_Semaforo(SEMKEY,3,&sem)==ERR){
    perror("Error al crear semaforo"); 
    }

 /* Inicializacion de todos los semaforos a 1*/
     if(Inicializar_Semaforo(sem,buf2)==ERR ) {
        perror("Error inicializar sem");
        Borrar_Semaforo(sem);
        exit(EXIT_FAILURE); 
     }

    for(i=0;i<100;i++) {/* Bucle para crear los 100 procesos hijos que representan a los coches*/
        pid = fork();
        if(pid == 0){

	    srand(i);
            sentido = i%2;
            sleep(rand()%10);

            LightSwitchOn(sem,sentido,2,&contador);/* El coche entra*/
            printf("\n El coche %d entra en el puente en el sentido %d\n",i,sentido);
            sleep(1);
	     printf("\n El Coche %d sale del puente\n", i);
            LightSwitchOff(sem,sentido,2,&contador);/*El coche sale*/
 
	    shmdt (contador);
            exit(1);   
        }else if (pid == -1){
            exit(-1);
        }
    } 
    if(pid) {   
    // Espera a todos los hijos que iran acabando.
    for(i=0;i<100;i++) {
        wait(&status);
    }    shmctl (id_zona, IPC_RMID, (struct shmid_ds *)NULL);/* Se libera el segmento de memoria compartida*/
    Borrar_Semaforo(sem);/* Se borra el semaforo creado*/
    exit(1);
    
    }

}

void LightSwitchOn(int bab, int sentido,int rec,int **contador){

    Down_Semaforo(bab,sentido,0);/* Bloquea los coches que van en su mismo sentido para no crear incongruencias*/
    (*contador)[sentido]++;/* Contador del numero de coches en el puente*/
    if ( (*contador)[sentido] == 1){
        Down_Semaforo(bab,rec,0);/* Una vez que ya haya un coche en el puente se bloquea el puente para los coches del sentido contrario*/
    }
    Up_Semaforo(bab,sentido,0);/* Desbloque los coches en su mismo sentido*/
}
void LightSwitchOff(int bab,int sentido,int rec,int **contador){

    Down_Semaforo(bab,sentido,0);/* Bloquea los coches que van en su mismo sentido para no crear incongruencias*/
    (*contador)[sentido]--;/* Contador del numero de coches en el puente*/
    if ( (*contador)[sentido] == 0){
        Up_Semaforo(bab,rec,0);/*Cuando no quedan coches dentro, desbloquea el puente para que entren coches en otro sentido*/
    }
    Up_Semaforo(bab,sentido,0);/* Desbloque los coches en su mismo sentido*/
}

