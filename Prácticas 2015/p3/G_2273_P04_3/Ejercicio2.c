#include <sys/wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>  
#include <sys/ipc.h> 
#include <string.h> 
#include <errno.h> 
#include <sys/shm.h> 
 
#define FILEKEY "/bin/cat" /*Util para ftok */ 
 
#define KEY 1300 
#define MAXBUF 10 

typedef struct _info{ 
	char nombre[80]; 
	int id; 
}info;

info * buffer; /* shared buffer */

/*Declaracion del manejador*/

void manejador_SIGUSR1 (int sig);
 
int main (int argc, char *argv[]) { 
int key, id_zone, N;
int i, estado;
pid_t aux, ppid;

if(argc!=2){
	fprintf(stderr, "Error en los parametros de entrada.");
	exit(-1);
}

/*Mapeo de las señales recibidas por el proceso padre*/
if(signal (SIGUSR1, manejador_SIGUSR1)==SIG_ERR){ 
 	perror("signal SIGUSR1"); 
 	exit(EXIT_FAILURE); 
}

N=atoi(argv[1]);

/* Key to shared memory */ 
 key = ftok(FILEKEY, KEY); 
 if (key == -1) { 
 fprintf (stderr, "Error with key \n"); 
 return -1; 
 } 
 
 /* We create the shared memory */ 
 id_zone = shmget (key, sizeof(info), IPC_CREAT | IPC_EXCL 
 |SHM_R | SHM_W); 
 if (id_zone == -1) { 
 fprintf (stderr, "Error with id_zone \n"); 
 exit(-1); 
 }
 printf ("ID zone shared memory: %i\n", id_zone); 
 
 
 /* we declared to zone to share */ 
 buffer = shmat (id_zone, (char *)0, 0); 
 if (buffer == NULL) { 
 fprintf (stderr, "Error reserve shared memory \n"); 
 exit(EXIT_FAILURE); 
 }

/*Inicializacion de buffer.id.*/
buffer->id=0;

for(i=0; i< N; i++){
	aux = fork();
	if(aux<0){
		fprintf(stdout, "Error en la llamada la funcion fork\n");
		exit(-1);
	}		
	if(aux==0){
		srand(i+time(NULL));
		sleep(rand()%30);
		fprintf(stdout, "Introduzca el nombre del cliente:\n");
		fscanf(stdin, " %s", buffer->nombre);

		(buffer->id)++;

		fprintf(stdout, "Envio de señal al padre por el proceso %d\n",i);
		/*Envia la señal SIGUSR1 al proceso padre.*/
		if(kill(getppid(), SIGUSR1)==-1){
			perror("Error kill signal SIGUSR1"); 
 			exit(EXIT_FAILURE); 	
		}
		
		shmdt(buffer);
		exit(0);	
	}	

}
	if(aux>0){
	for(i=0; i<N; i++){
		/* Bloquea al proceso hasta que llegue una señal SIGUSR1 de algun hijo*/
		pause(); 
		wait(&estado);
	}
	}
	
	/*Separamos la direccion de memoria compartida.*/
    shmdt(buffer);
 
	/*Y eliminamos del sistema el identificador de memoria compartida.*/
    shmctl(id_zone, IPC_RMID, (struct shmid_ds *)NULL);
  
    exit(1);
}

/*Manejador de la señal SIGUSR1 para todos los hijos*/
void manejador_SIGUSR1 (int sig){
	fprintf(stdout, "%s (%d).\n", buffer->nombre, buffer->id);	
}
