#include <stdio.h>
#include "semaforos.h"

#define SEMKEY 75798
#define SEMKEY2 70236
#define N_SEMAFOROS 2
#define MUTEX 0
#define MUTEX2 1

int main(void)
{
	int i, pid;
	int sem=ERR;
	unsigned short buf[2]={0,1};
	
	/* Creamos e inicializamos el semaforo y el mutex, que compartiran el padre y los hijos*/ 
	
	if (Crear_Semaforo(SEMKEY, 2, &sem)==ERR) {
		perror("Error crear semaforo");
		exit(EXIT_FAILURE);
	}
	
	if(Inicializar_Semaforo(sem,buf)==ERR ) {
		perror("Error inicializar sem");
		Borrar_Semaforo(sem);
		exit(EXIT_FAILURE);	
	}
	
pid = fork();
	if(pid == -1){
	error("Error.");
	}
	
	else if (pid == 0) /*hijo*/{
		
		for(i=0; i < 5; i++) {
		Down_Semaforo(sem,MUTEX,1);
		printf ("Soy el hijo %d \n",i);
		Up_Semaforo(sem,MUTEX2,1);
		}
	}
	
	else /* padre */ {
		for(i = 0;i < 5; i++){
		Down_Semaforo(sem,MUTEX2,1);
		printf ("Soy el padre: %d \n",i);
		Up_Semaforo(sem,MUTEX,1);
		sleep(5);
		}

	}
	
	Borrar_Semaforo(sem);
}