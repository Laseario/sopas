
/** * @brief Solucion al ejercicio 10 de la Practica 2 de SOPER
    * 
    * Este programa genera un proceso hijo, que tiene que ejecutar la orden
    * suministrada en linea de comandos, mientras que el proceso padre
    * debe esperar 10 segundos y cuando se pasa el tiempo termina el 
    * programa escribiendo un mensaje por pantalla (Tiempo agotado)
    *
    * @file ejercicio10.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#define SECS 10

void captura(int sennal) 
{ 
 printf("\nTiempo agotado.\n", SECS); 
 
 exit(0); 
}
 
int main(int argc, char **argv){
 
    pid_t aux;
    int i, status, estado;
     
    if(argc!=2){
	printf("Error en los argumentos de entrada\n");
        return -1;  
    }
        aux = fork();
 
        if(aux<0){
            fprintf(stdout,"Error en la llamada al proceso fork\n");
            return -1;
        }

        else if(aux==0){
	    /*Ejecuta la orden suministrada en línea de comandos.*/
            execlp(argv[1], argv[1], NULL);
	    pause();
	    exit(0);
        }


     if (signal(SIGALRM, captura) == SIG_ERR){ 
     puts("Error en la captura"); 
     exit (EXIT_FAILURE); 
     } 

     if (alarm(SECS)) 
     fprintf(stderr, "Existe una alarma previa establecida\n"); 

     pause();
     //termina con el hijo:
     kill (-aux, SIGKILL);   
        wait(&estado);
    exit(0);

    return 0;
}
