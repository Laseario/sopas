
/** * @brief Solucion al ejercicio 3a de la Practica 2 de SOPER
    * 
    * Este programa genera NUMHIJOS procesos hijos a partir de un padre
    * y después cada hijo muestra por pantalla un numero aleatorio entre
    * 0 y RAND_MAX
    *
    * @file ejercicio3a.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/resource.h>

#define MILLION 1000000
#define NUMHIJOS 100

int main(){

	int i, estado;
	pid_t aux;
	double a=0.0000000, b=0.0000000;
    	double res = 0;
    	struct timespec start, stop;
	/*acotamos lo que queremos medir, en start*/
	clock_gettime( CLOCK_REALTIME, &start);
	for(i=0; i< NUMHIJOS; i++){
		srand(i);
		aux = fork();
		if(aux<0){
			fprintf(stdout, "Error en la llamada la funcion fork\n");
			return -1;
		}		
		if(aux==0){
			fprintf(stdout,"%d \n", rand());
			exit(0);
			break;
			
		}	

	}
	if(aux>0){	
		for(i=0; i<NUMHIJOS; i++){
			wait(&estado);
		}
	}

	clock_gettime( CLOCK_REALTIME, &stop);
    
	/*Calculamos el tiempo que se invierte.*/
    	res = (stop.tv_sec-start.tv_sec)*MILLION + (stop.tv_nsec - start.tv_nsec);

	/*Los pasamos a segundos.*/
	res=res/(double)MILLION; 

	fprintf(stdout,"El tiempo que ha tardado el padre en crear %d procesos hijo es: %.7lf\n", NUMHIJOS, res);

	return 0;
}
	

