
/** * @brief Solucion al ejercicio 6 de la Practica 2 de SOPER
    * 
    * Este programa genera 1 proceso hijo que permanentemente
    * pone por pantalla un mensaje y duerme 5 sec.
    * Hasta que el padre le envie una signal de terminacion
    *
    * @file ejercicio6.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
 
int main(){
 
    int i, estado, pid;
    pid_t aux;

        aux = fork();
        if(aux<0){
            fprintf(stdout, "Error en la llamada la funcion fork\n");
            return -1;
        }       
        if(aux==0){
	    /*queremos que el hijo siga ejecutandose, lo hará 6 veces en total.*/
	    while(1){
            fprintf(stdout,"Soy el proceso hijo con PID:%d\n", getpid());
	    sleep(5);
	    
	}
	
	exit(0);
	}
	
	/*el proceso padre espera 30 unidades de tiempo.*/
	sleep(30);
	/*envía una señal al hijo.*/
	kill (aux, SIGINT);   
        wait(&estado);
	exit(0);
 
return 0;
}
