/** * @brief Solucion al ejercicio 3b de la Practica 2 de SOPER
    * 
    * Este programa genera NUMHILOS hilos a partir de un padre
    * y después cada hilo muestra por pantalla un numero aleatorio entre
    * 0 y RAND_MAX
    *
    * @file ejercicio3b.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */
 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 
#include <time.h>
#include <pthread.h>
 
#define NUMHILOS 100
#define MILLION 1000000
 
 
 
void *print_rand(void *arg){
    int i = 0;
    i = (int)arg;
    srand(i);
    fprintf(stdout,"%d \n", rand());
    pthread_exit(NULL); 
}
int main(){
     
    int i;
    pthread_t hilo[NUMHILOS];
 
    double tiempo = 0;
        struct timespec start, stop;
    /*acotamos lo que queremos medir, en start*/
    clock_gettime( CLOCK_REALTIME, &start);
 
    for(i=0; i< NUMHILOS; i++){
         
        pthread_create(&hilo[i], NULL, print_rand, i);
    }
     
    for(i=0; i< NUMHILOS; i++){
         
        pthread_join(hilo[i], NULL);
    }
 
    clock_gettime( CLOCK_REALTIME, &stop);
     
    /*Calculamos el tiempo que se invierte.*/
        tiempo = (stop.tv_sec-start.tv_sec)*MILLION + (stop.tv_nsec - start.tv_nsec);
 
    /*Los pasamos a segundos.*/
    tiempo=tiempo/(double)MILLION; 
 
    fprintf(stdout,"El tiempo que ha tardado el padre en crear %d hilos es: %.7lf\n", NUMHILOS, tiempo);
     
    return 0;
}
