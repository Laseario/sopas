
/** * @brief Solucion al ejercicio 8 de la Practica 2 de SOPER
    * 
    * Este programa genera 4 procesos hijo de manera
    * recurrente que despues desplegaran un sistema
    * circular de envio de señales de padres a hijos y de 
    * hijos a padres.
    * 3 veces se enviara una misma signal entre todos
    * para finalmente todos terminar con la recepcion
    * de la ultima signal.
    *
    * @file ejercicio6.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */

#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#define NUMHIJOS 4

/*Declaracion de todos los manejadores*/
void manejador_SIGUSR1 (int sig);
void manejador_SIGUSR1_Raiz( int sig);
void manejador_SIGUSR2 (int sig); 
void manejador_SIGUSR2_Raiz( int sig); 
void manejador_SIGTERM (int sig); 
void manejador_SIGTERM_Raiz( int sig);  


int main(){

	int i, estado;
	pid_t aux;
	pid_t raiz = getpid();

	for(i=0; i< NUMHIJOS; i++){

		if(i==0){/*Mapeo de las señales recibidas por el proceso raiz*/
			if(signal (SIGUSR1, manejador_SIGUSR1_Raiz)==SIG_ERR){ 
	 			perror("signal SIGUSR1 en Raiz"); 
	 			exit(EXIT_FAILURE); 
			}
			if(signal (SIGUSR2, manejador_SIGUSR2_Raiz)==SIG_ERR){ 
	 			perror("signal SIGUSR2 en Raiz"); 
	 			exit(EXIT_FAILURE); 
			}
			if(signal (SIGTERM, manejador_SIGTERM_Raiz)==SIG_ERR){ 
	 			perror("signal SIGTERM en Raiz"); 
	 			exit(EXIT_FAILURE); 
			}
		}
		else{/*Mapeo de los manejadores de las señales recibidas por todos los hijos*/
			if(signal (SIGUSR1, manejador_SIGUSR1)==SIG_ERR){ 
	 			perror("signal SIGUSR1"); 
	 			exit(EXIT_FAILURE); 
			}
			if(signal (SIGUSR2, manejador_SIGUSR2)==SIG_ERR){ 
	 			perror("signal SIGUSR2"); 
	 			exit(EXIT_FAILURE); 
			}
			if(signal (SIGTERM, manejador_SIGTERM)==SIG_ERR){ 
	 			perror("signal SIGTERM"); 
	 			exit(EXIT_FAILURE); 
			}
		}

		aux = fork();
		if(aux<0){/*Control de Errores*/
			fprintf(stdout, "Error en la llamada la funcion fork\n");
            		return -1;
		}
		if(aux>0){/*Linea que sirve para generar los hijos en cascada*/
			break;
		}
	}


	if(aux==0){
		/*codigo del hijo hoja*/
		sleep(5);
		if(kill(getppid(), SIGUSR1)==-1){
			perror("Error kill signal SIGUSR1"); 
	 		exit(EXIT_FAILURE); 	
		}
		pause();/* Bloquea al proceso hasta que llegue una señal*/ 
		if(kill(getppid(), SIGUSR2)==-1){
			perror("Error kill signal SIGUSR2"); 
	 		exit(EXIT_FAILURE); 	
		}
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGTERM del padre*/ 
		if(kill(raiz, SIGTERM)==-1){
			perror("Error kill signal SIGTERM");
		}
		exit(0);
	}
	if(getpid() ==raiz){
		/* Codigo del proceso raiz */
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGUSR1 del hijo*/
		if(kill(aux, SIGUSR2)==-1){
			perror("Error kill signal SIGUSR2"); 
	 		exit(EXIT_FAILURE); 	
		}
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGUSR2 del ultimo hijo*/ 
		if(kill(aux, SIGTERM)==-1){
			perror("Error kill signal SIGTERM");
		}
		pause();
	
	} 
	else if(aux){
		/* codigo del nodo hoja*/
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGUSR1 del hijo*/ 
		if(kill(getppid(), SIGUSR1)==-1){
			perror("Error kill signal SIGUSR1"); 
	 		exit(EXIT_FAILURE); 	
		}
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGUSR2 del padre*/ 
		if(kill(aux, SIGUSR2)==-1){
			perror("Error kill signal SIGUSR2"); 
	 		exit(EXIT_FAILURE); 	
		}
		pause(); /* Bloquea al proceso hasta que llegue una señal SIGTERM del padre*/ 
		if(kill(aux, SIGTERM)==-1){
			perror("Error kill signal SIGTERM");
		}
		exit(0);

	}
	return 0;
}




/*Manejador de la señal SIGUSR1 para todos los hijos*/
void manejador_SIGUSR1 (int sig){
	fprintf(stdout, "Proceso %d recibe la señal SIGUSR1\n", getpid());	
	sleep(2);
} 

/*Manejador de la señal SIGUSR1 del proceso raiz*/
void manejador_SIGUSR1_Raiz( int sig){
	fprintf(stdout, "Proceso padre recibe la señal SIGUSR1\n");
	sleep(2);
}

/*Manejador de la señal SIGUSR2 para todos los hijos*/
void manejador_SIGUSR2 (int sig){
	fprintf(stdout, "Proceso %d recibe la señal SIGUSR2\n", getpid());	
	sleep(1);
} 

/*Manejador de la señal SIGUSR2 del proceso raiz*/
void manejador_SIGUSR2_Raiz( int sig){
	fprintf(stdout, "Proceso padre recibe la señal SIGUSR2\n");	
	sleep(1);
}

/*Manejador de la señal SIGTERM para todos los hijos*/
void manejador_SIGTERM (int sig){
	fprintf(stdout, "Proceso %d recibe la señal SIGTERM\n", getpid());	
	sleep(1);
} 

/*Manejador de la señal SIGTERM del proceso raiz*/
void manejador_SIGTERM_Raiz( int sig){
	fprintf(stdout, "Proceso padre recibe la señal SIGTERM\n");	
	exit(0);
}  

	
