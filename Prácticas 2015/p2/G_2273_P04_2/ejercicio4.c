
/** * @brief Solucion al ejercicio 4 de la Practica 2 de SOPER
    * 
    * Este programa genera 2 hilos a partir de un proceso
    * y después cada hilo devuelve su TID que finalmente sera
    * mostrado por pantalla.
    *
    * @file ejercicio4.c
    * @authors Lucas Milla y Jorge García
    * @date 20-02-2015 */ 

#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h> 

#define NUM_HILOS 2 
  
char mensaje_global[50]; 
  
void *funcion_hilo(void *id) 
{ 
sprintf(mensaje_global, "Hola soy el TID %ld", (long int)syscall(SYS_gettid));
pthread_exit((void*)mensaje_global); 
}
  
int main() 
{ 
 char *valor_devuelto= NULL;
 int i;

 pthread_t tid; 
 
 for (i=0;i<NUM_HILOS;i++) 
 { 
 pthread_create(&tid, NULL, funcion_hilo,NULL); 
 pthread_join(tid, (void**)&valor_devuelto);
 fprintf(stdout, "%s\n", valor_devuelto);
 } 
} 
