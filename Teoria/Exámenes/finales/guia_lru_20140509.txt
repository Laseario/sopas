Planificadores de memoria (examen 06/09/2014, ej2 apartado A)
=============================================================

1. El campo de página corresponde a la cifra hexadecimal de la dirección de página (en el ejemplo los 6 primeros bits de cada acceso realizado).
2. Se asigna a uno de los marcos utilizados dependiendo del algoritmo usado, la parte de fallo de página se marca si la dirección no se encontraba en ningún marco antes de ser asignada.

Ejercicios direcciones físicas con planificadores (examen 06/09/2014, ej2 apartado B)
=====================================================================================

1. Hay que tener en cuenta la configuración de los marcos que nos dan, cual es la página asignada a cada uno, en todo momento.
2. Cuando nos dan una dirección hay que buscarla entre los valores de los marcos. Si están en uno (p. ej. si el segundo marco, el 0x21, contiene el valor 0x3F para cuando leemos la entrada 0xFFFF) cogemos su dirección, esa será los primeros bits de la dirección resultado (en el ejercicio son 6 bits, así que nuestro puntero empieza por b100001, que con el offset b11 1111 1111 da una dirección física 0x87FF) y lo concatenamos con los bits del offset. Si no se encuentra en ninguno de los marcos da error de página.